#include"data.hpp"
#include"httplib.h"

namespace order_sys{
#define SERVER_PORT 20000
#define WWWROOT "./wwwroot"
  class Server{
    private:

      int _port=SERVER_PORT;
      std::string _wwwroot=WWWROOT;
      httplib::Server *_server;
      static TableDish *_table_dish;
      static TableOrder *_table_order;
    private:


      static void DishInsert(const httplib::Request &req,httplib::Response &rsp){
      //获取新增信息（保存在请求正文中  req.bady）
      //对正文进行json反序列化
        Json::FastWriter writer; 
        Json::Value dish;
        Json::Reader reader;
        Json::Value err;
        bool ret=reader.parse(req.body,dish);
        if(ret==false){
          err["result"]=false;
          err["reason"]="提交信息有误";
          rsp.status=400;
          rsp.body=writer.write(err);
          rsp.set_header("Content-Type","application/json");
          std::cout<<rsp.body<<std::endl;
          return ;
        }
      //调用数据管理模块对象，实现数据到数据库中的新增
      ret= _table_dish->Insert(dish);
      if(ret==false){
        err["result"]=false;
        err["reason"]="插入失败";
        rsp.status=500;
        rsp.body=writer.write(err);
        rsp.set_header("Content-Type","application/json");
        std::cout<<rsp.body<<std::endl;
        return ;
      }
      //返回执行结果
      rsp.status=200;
      }
      static void DishDelete(const httplib::Request &req,httplib::Response &rsp){
      //明确删除的信息
      int dish_id=std::stoi(req.matches[1]);
      //调用数据管理模块，从数据库中删除数据
      bool ret=_table_dish->Delete(dish_id);
      if(ret==false){
        Json::FastWriter writer;
        Json::Value err;
        err["result"]=false;
        err["reason"]="菜品信息删除错误";
        rsp.status=500;
        rsp.body=writer.write(err);
        rsp.set_header("Content-Type","application/json");
        std::cout<<rsp.body<<std::endl;
        return ;
      }
      //相应处理结果
      rsp.status=200;
      }
      static void DishUpdate(const httplib::Request &req,httplib::Response &rsp){
      //获取修改的id
      int dish_id=std::stoi(req.matches[1]);
      //获取新的信息
      Json::FastWriter writer;
      Json::Value dish;
      Json::Reader reader;
      Json::Value err;
      bool ret=reader.parse(req.body,dish);
      if(ret==false){
        err["result"]=false;
        err["reason"]="提交信息有误";
        rsp.status=400;
        rsp.body=writer.write(err);
        rsp.set_header("Content-Type","application/json");
        std::cout<<rsp.body<<std::endl;
        return ;
      }
      //对数据库中的信息更新
      ret=_table_dish->Update(dish_id,dish);
      if(ret==false){
        err["relust"]=false;
        err["reason"]="更新失败";
        rsp.status=500;
        rsp.body=writer.write(err);
        rsp.set_header("Content-Type","application/json");
        std::cout<<rsp.body<<std::endl;
        return ;
      }
      //返回处理结果
      rsp.status=200;
      }
      static void DishGetAll(const httplib::Request &req,httplib::Response &rsp){
      //从数据库获取出所有信息
        Json::FastWriter writer;
        Json::Value dish;
        Json::Value err;
      bool ret=_table_dish->GetAll(&dish);
      if(ret==false){
        err["result"]=false;
        err["reason"]="从数据库中获取所有菜品信息失误";
        rsp.status=500;
        rsp.body=writer.write(err);
        rsp.set_header("Content-Type","application/json");
        std::cout<<rsp.body<<std::endl;
        return ;
      }
      //将菜品信息序列化为字符串
      rsp.body=writer.write(dish);
      rsp.status=200;
      rsp.set_header("Content-Type","application/json");
      }
      static void DishGetOne(const httplib::Request &req,httplib::Response &rsp){
        int dish_id=std::stoi(req.matches[1]);
        Json::FastWriter writer;
        Json::Value dish;
        Json::Value err;
        bool ret=_table_dish->GetOne(dish_id,&dish);
        if(ret==false){
          err["result"]=false;
          err["reason"]="从数据库获取信息失败";
          rsp.status=500;
          rsp.body=writer.write(err);
          rsp.set_header("Content-Type","application/json");
          std::cout<<rsp.body<<std::endl;
          return ;
        }
        rsp.body=writer.write(dish);
        rsp.status=200;
        rsp.set_header("Content-Type","application/json");
      }
      
      //订单
      static void OrderInsert(const httplib::Request &req,httplib::Response &rsp){
      //获取新增信息（保存在请求正文中  req.bady）
      //对正文进行json反序列化
        Json::FastWriter writer;
        Json::Reader reader;
        Json::Value order;
        Json::Value err;
        bool ret=reader.parse(req.body,order);
        if(ret==false){
          err["result"]=false;
          err["reason"]="提交信息有误";
          rsp.status=400;
          rsp.body=writer.write(err);
          rsp.set_header("Content-Type","application/json");
          std::cout<<rsp.body<<std::endl;
          return ;
        }
      //调用数据管理模块对象，实现数据到数据库中的新增
      ret = _table_order->Insert(order);
      if(ret==false){
        err["result"]=false;
        err["reason"]="插入失败";
        rsp.status=500;
        rsp.body=writer.write(err);
        rsp.set_header("Content-Type","application/json");
        std::cout<<rsp.body<<std::endl;
        return ;
      }
      //返回执行结果
      rsp.status=200;
      }
      static void OrderDelete(const httplib::Request &req,httplib::Response &rsp){
      //明确删除的信息
      int order_id=std::stoi(req.matches[1]);
      //调用数据管理模块，从数据库中删除数据
      bool ret=_table_order->Delete(order_id);
      if(ret==false){
        Json::FastWriter writer;
        Json::Value err;
        err["result"]=false;
        err["reason"]="订单信息删除错误";
        rsp.status=500;
        rsp.body=writer.write(err);
        rsp.set_header("Content-Type","application/json");
        std::cout<<rsp.body<<std::endl;
        return ;
      }
      //相应处理结果
      rsp.status=200;
      }
      static void OrderUpdate(const httplib::Request &req,httplib::Response &rsp){
      //获取修改的id
      int order_id=std::stoi(req.matches[1]);
      //获取新的信息
      Json::FastWriter writer;
      Json::Value order;
      Json::Reader reader;
      Json::Value err;
      bool ret=reader.parse(req.body,order);
      if(ret==false){
        err["result"]=false;
        err["reason"]="提交信息有误";
        rsp.status=400;
        rsp.body=writer.write(err);
        rsp.set_header("Content-Type","application/json");
        std::cout<<rsp.body<<std::endl;
        return ;
      }
      //对数据库中的信息更新
      ret=_table_order->Update(order_id,order);
      if(ret==false){
        err["relust"]=false;
        err["reason"]="更新失败";
        rsp.status=500;
        rsp.body=writer.write(err);
        rsp.set_header("Content-Type","application/json");
        std::cout<<rsp.body<<std::endl;
        return ;
      }
      //返回处理结果
      rsp.status=200;
      }
      static void OrderGetAll(const httplib::Request &req,httplib::Response &rsp){
      //从数据库获取出所有信息
        Json::FastWriter writer;
        Json::Value order;
        Json::Value err;
      bool ret=_table_order->GetAll(&order);
      if(ret==false){
        err["result"]=false;
        err["reason"]="从数据库中获取所有订单信息失误";
        rsp.status=500;
        rsp.body=writer.write(err);
        rsp.set_header("Content-Type","application/json");
        std::cout<<rsp.body<<std::endl;
        return ;
      }
      //将订单信息序列化为字符串
      rsp.body=writer.write(order);
      rsp.status=200;
      rsp.set_header("Content-Type","application/json");
      }
      static void OrderGetOne(const httplib::Request &req,httplib::Response &rsp){
        int order_id=std::stoi(req.matches[1]);
        Json::FastWriter writer;
        Json::Value order;
        Json::Value err;
        bool ret=_table_order->GetOne(order_id,&order);
        if(ret==false){
          err["result"]=false;
          err["reason"]="从数据库获取信息失败";
          rsp.status=500;
          rsp.body=writer.write(err);
          rsp.set_header("Content-Type","application/json");
          std::cout<<rsp.body<<std::endl;
          return ;
        }
        rsp.body=writer.write(order);
        rsp.status=200;
        rsp.set_header("Content-Type","application/json");
      }


    
    public:
      Server(int port=SERVER_PORT):_port(port){

      }
      bool RunServer(){
        _table_dish=new TableDish();
        _table_order=new TableOrder();
        //实例化httplib的server 对象
        _server=new httplib::Server();
        
        //设置静态资源目录
        _server->set_mount_point("/",_wwwroot);
      
        //当服务器收到了post请求方法的/dish请求则使用DishInsert函数进行处理
        _server->Post(R"(/dish)",DishInsert);
        _server->Delete(R"(/dish/(\d+))",DishDelete); 
        _server->Put(R"(/dish/(\d+))",DishUpdate); 
        _server->Get(R"(/dish)",DishGetAll); 
        _server->Get(R"(/dish/(\d+))",DishGetOne); 
        
        _server->Post(R"(/order)",OrderInsert);
        _server->Delete(R"(/order/(\d+))",OrderDelete); 
        _server->Put(R"(/order/(\d+))",OrderUpdate); 
        _server->Get(R"(/order)",OrderGetAll); 
        _server->Get(R"(/order/(\d+))",OrderGetOne); 

        _server->listen("0.0.0.0",_port);
        return true;

      }
  };
  TableDish *Server::_table_dish=nullptr;
  TableOrder *Server::_table_order=nullptr;
}
