#include<stdio.h>
#include<unistd.h>
#include<string.h>
#include<mysql/mysql.h>

int main(){
  //初始化句柄
  MYSQL *mysql=mysql_init(NULL);
  if(mysql==NULL){
    printf("mqsql init error\n");
    return -1;
  }
  //链接数据库服务器
  if(mysql_real_connect(mysql,"127.0.0.1","root","1","lei",0,NULL,0)==NULL){
    printf("connect mysql server failed!:%s\n",mysql_error(mysql));
    mysql_close(mysql);
    return -1;
  }
  //设置字符集
  int ret;
  ret=mysql_set_character_set(mysql,"utf8");
  if(ret!=0){
  printf("set character failed!:%s\n",mysql_error(mysql));
  mysql_close(mysql);
  return -1;
  }
  //选择要操作的库
  //mysql_select_db(mysql,"lei");
  //执行语句
  const char *insert="insert student value(4,'张三',23,now());";
    ret=mysql_query(mysql,insert);
    if(ret!=0){
       printf("query sql failed!:%s\n",mysql_error(mysql));
       mysql_close(mysql);
       return -1;
    }
  //如果是查询语句则获得查询结果
  MYSQL_RES *res=mysql_store_result(mysql);
  if(res==NULL){
    printf("store result failed!:%s\n",mysql_error(mysql));
    mysql_close(mysql);
    return -1;
  }
  int num_row=mysql_num_rows(res);
  int num_col=mysql_num_fields(res);
  for(int i=0;i<num_row;i++){
    MYSQL_ROW row=mysql_fetch_row(res);
    for(int j=0;j<num_col;j++){
      printf("%-15s",row[j]);
    }
    printf("\n");
  }
  //释放结果集
  mysql_free_result(res);
  //关闭
  mysql_close(mysql);
  return 0;
}
