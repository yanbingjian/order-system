#include<iostream>
#include<string>
#include<jsoncpp/json/json.h>

void Serialize(){
  const char *name="张三";
  int age=18;
  float score[]={88.5,99,45};

  Json::Value val;
  val["姓名"]=name;
  val["年龄"]=age;
  val["成绩"].append(score[0]);
  val["成绩"].append(score[1]);      
  val["成绩"].append(score[2]);      
//  Json::StyledWriter writer;
  Json::FastWriter writer;

  std::string str=writer.write(val);
  std::cout<<str<<std::endl;
}
void UnSerialize(){
  std::string str=R"({"姓名":"李四","年龄":19,"成绩":[66,87.5,99]})";
  std::cout<<str<<std::endl;
  Json::Reader reader;
  Json::Value val;
  bool ret=reader.parse(str,val);
  if(ret==false){
    std::cout<<"json parse error\n";
    return ;
  }
  std::cout<<val["姓名"].asString()<<std::endl;
  std::cout<<val["年龄"].asInt()<<std::endl; 
	if(val["成绩"].isArray()){
		int se=val["成绩"].size(); 
		for(int i=0;i<se;i++){
				std::cout<<val["成绩"][i].asFloat()<<std::endl;
		}
	}
  return ;
}
int main(){
  UnSerialize();
  Serialize();
  return 0;
}
