#include"httplib.h"
//对/hi请求的处理方法
void HelloWorld(const httplib::Request &req,httplib::Response &res)
{
  res.body="Hello World";
  res.status=200;
}
//对/number/正则表达式请求的处理方法
void Numbers(const httplib::Request &req,httplib::Response &res)
{
  std::string num=req.matches[1]; //matches[1] 是(\d+)
  num+="\n";
  num+=req.matches[0];  //matches[0] 是/number/(\d+)
  res.body=num;
  res.status=200;
}
//对/dish请求的处理方法,
void Dish(const httplib::Request &req,httplib::Response &res)
{
  res.body=req.body;
  res.status=200;
}
int main()
{
  httplib::Server srv;

  srv.set_mount_point("/","./wwwroot");   //设置静态资源根目录，/index.html   ->  ./wwwroot/index.html
  srv.Get("/hi",HelloWorld);   //Get /hi HTTP/1.1
  //正则表达式:字符串的匹配机制，\d+ 表示匹配一个或多个连续的数字字符
  srv.Get(R"(/number/(\d+))",Numbers);  //Get /number/123 HTTP/1.1
  srv.Post("/dish",Dish);

  //搭建tcp服务器，开始监听，获取新建连接，接受http请求数据，进行解析，调用对应的处理函数，组织响应
  srv.listen("0.0.0.0",9000);
  return 0;
}
