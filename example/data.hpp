#include<iostream>
#include<cstdlib>
#include<string>
#include<jsoncpp/json/json.h>
#include<mutex>
#include<mysql/mysql.h>

namespace order_sys{
#define DB_HOST "127.0.0.1"
#define DB_USER "root"
#define DB_PASS "1"
#define DB_NAME "lei"
  //返回一个初始化完成的MySQL操作句柄
  MYSQL *MysqlInit(){
    //初始化句柄
    MYSQL *mysql=mysql_init(NULL);
    if(mysql==NULL){
      std::cout<<"init mysql handler failed!\n";
      return NULL;
    }
    //连接MySQL服务器
    if(mysql_real_connect(mysql,DB_HOST,DB_USER,DB_PASS,DB_NAME,0,NULL,0)==NULL){
      std::cout<<"connect mysql server failed:"<<mysql_error(mysql)<<std::endl;
      mysql_close(mysql);
      return NULL;
    }
    //设置字符集
    if(mysql_set_character_set(mysql,"utf8")!=0){
      std::cout<<"set client character failed!\n"<<mysql_error(mysql)<<std::endl;
      mysql_close(mysql);
      return NULL;
    }
    //返回句柄
    return mysql;
  }
  //mysql句柄销毁接口
  void MysqlDestroy(MYSQL *mysql){
    if(mysql!=NULL){
      mysql_close(mysql);
    }
    return ;
  }
  //Mysql句柄执行接口
  bool MysqlQuery(MYSQL *mysql,const std::string &sql){
    int ret=mysql_query(mysql,sql.c_str());
    if(ret!=0){
      std::cout<<sql<<std::endl;
      std::cout<<"query failed:"<<mysql_error(mysql)<<std::endl;
      return false;
    }
    return true;
  }
  class TableDish{
    private:
      std::mutex _mutex;
      MYSQL *_mysql;
    public:
      //初始化数据库操作句柄
      TableDish():_mysql(NULL){
        _mysql=MysqlInit();
        if(_mysql==NULL){
          exit(-1);
        }
      }
      //释放销毁数据库操作句柄
      ~TableDish(){
        MysqlDestroy(_mysql);
      }
      //dish传入新增菜品信息
      bool Insert(const Json::Value &dish){
        if(dish["name"].empty()==true||dish["name"].asString().size()==0){
          std::cout<<"new dish name is not exists!\n";
          return false;
        }
        if(dish["price"].empty()==true||dish["price"].asInt()<=0){
          std::cout<<"new dish price is error!\n";
          return false;
      }
        if(dish["info"].empty()==true||dish["info"].asString().size()==0){
          std::cout<<"new dish info is null!\n";
          return false;
      }
        char sql[4096]={0};
#define INSERT_DISH "insert td_dish values(null,'%s',%d,'%s',now());"
        sprintf(sql,INSERT_DISH,dish["name"].asCString(),dish["price"].asInt(),dish["info"].asCString());
        return MysqlQuery(_mysql,sql);
  }
      //修改菜品信息
      bool Update(int dish_id,const Json::Value &dish){
        char sql[4096]={0};
#define UPDATE_DISH "update td_dish set name='%s',price=%d,info='%s',ctime=now() where id=%d;"
        sprintf(sql,UPDATE_DISH ,dish["name"].asCString(),dish["price"].asInt(),dish["info"].asCString(),dish_id);
      return MysqlQuery(_mysql,sql);
      }
      //删除菜品信息
      bool Delete(int dish_id){
#define DELETE_DISH "delete from td_dish where id=%d;"
      char sql[4096]={0};
      sprintf(sql,DELETE_DISH,dish_id);
      return MysqlQuery(_mysql,sql);
      }
      //dishes作为输出参数返回所有菜品信息
      bool GetAll(Json::Value *dishes){
#define SELECT_ALLDISH "select id,name,price,info,ctime  from td_dish;"
        _mutex.lock();
        bool ret=MysqlQuery(_mysql,SELECT_ALLDISH);
        if(ret==false){
          _mutex.unlock();
          return false;
        }
        MYSQL_RES *res=mysql_store_result(_mysql);
        if(res==NULL){
          printf("store result failed!:%s\n",mysql_error(_mysql));
          _mutex.unlock(); 
          return -1;
        }
        _mutex.unlock(); 
        int num_row=mysql_num_rows(res);
        if(num_row<0){
          std::cout<<"have no dish"<<std::endl;
          return false;
        }
        for(int i=0;i<num_row;i++){
          MYSQL_ROW row=mysql_fetch_row(res);
          Json::Value dish;
          dish["id"]=atoi(row[0]);
          dish["name"]=row[1];   
          dish["price"]=atoi(row[2]);   
          dish["info"]=row[3];  
          dish["ctime"]=row[4];
          dishes->append(dish);//将单个菜品信息作为数组元素添加到输出参数DISH中
          }
        //释放结果集
          mysql_free_result(res);
          return true;
      }
      //返回指定菜品信息
      
      bool GetOne(int dish_id,Json::Value *dish){
#define SELECT_ONEDISH "select name,price,info,ctime from td_dish where id=%d;"
     char sql[4096]={0};
     sprintf(sql,SELECT_ONEDISH,dish_id);
     _mutex.lock(); 
     bool ret=MysqlQuery(_mysql,sql);
        if(ret==false){
          _mutex.unlock();
          return false;
        }
        MYSQL_RES *res=mysql_store_result(_mysql);
        if(res==NULL){
          printf("store result failed!:%s\n",mysql_error(_mysql));
          _mutex.unlock(); 
          return false;
        }
        _mutex.unlock(); 
        int num_row=mysql_num_rows(res);
        if(num_row<1){
          std::cout<<"have no dish:"<<dish_id<<std::endl;
          return false;
        }
          MYSQL_ROW row=mysql_fetch_row(res);
          (*dish)["id"]=dish_id;
          (*dish)["name"]=row[0];   
          (*dish)["price"]=atoi(row[1]);   
          (*dish)["info"]=row[2];  
          (*dish)["ctime"]=row[3];
  
          mysql_free_result(res);
          return true;
      }
  };

  class TableOrder{
    private:
      std::mutex _mutex;
      MYSQL *_mysql;
    public:
      TableOrder():_mysql(NULL){
        _mysql=MysqlInit();
        if(_mysql==NULL){
          exit(-1);
        }
      }

      ~TableOrder(){
        MysqlDestroy(_mysql);
      }

      bool Insert(const Json::Value &order){
        char sql[4096]={0};
#define INSERT_ORDER "insert td_order values(null,'%s',0,now(),now());"
        std::string dishes;
        Json::FastWriter writer;
        if(order["dishes"].isArray()){
          dishes=writer.write(order["dishes"]);
        }else{
          dishes=order["dishes"].asString();
        }
        sprintf(sql,INSERT_ORDER,dishes.c_str());
        return MysqlQuery(_mysql,sql);
  }

      bool Update(int order_id,const Json::Value &order){
        char sql[4096]={0};
#define UPDATE_ORDER "update td_order set status=%d,mtime=now() where id=%d;"
        sprintf(sql,UPDATE_ORDER ,order["status"].asInt(),order_id);
      return MysqlQuery(_mysql,sql);
      }

      bool Delete(int order_id){
#define DELETE_ORDER "delete from td_order where id=%d;"
      char sql[4096]={0};
      sprintf(sql,DELETE_ORDER,order_id);
      return MysqlQuery(_mysql,sql);
      }

      bool GetAll(Json::Value *orderes){
#define SELECT_ALLORDER "select id,dishes,status,ctime,mtime  from td_order;"
        _mutex.lock();
        bool ret=MysqlQuery(_mysql,SELECT_ALLORDER);
        if(ret==false){
          _mutex.unlock();
          return false;
        }
        MYSQL_RES *res=mysql_store_result(_mysql);
        if(res==NULL){
          printf("store result failed!:%s\n",mysql_error(_mysql));
          _mutex.unlock(); 
          return -1;
        }
        _mutex.unlock(); 
        int num_row=mysql_num_rows(res);
        if(num_row<1){
          std::cout<<"have no order"<<std::endl;
            return false;
        }
        for(int i=0;i<num_row;i++){
          MYSQL_ROW row=mysql_fetch_row(res);
          Json::Value order;
          order["id"]=atoi(row[0]);
          order["dishes"]=row[1];   
          order["status"]=atoi(row[2]);   
          order["ctime"]=row[3];  
          order["mtime"]=row[4];
         orderes->append(order);
          }
          mysql_free_result(res);
          return true;
      }

      bool GetOne(int order_id,Json::Value *order){
#define SELECT_ONEORDER "select dishes,status,ctime,mtime from td_order where id=%d;"
     char sql[4096]={0};
     sprintf(sql,SELECT_ONEORDER,order_id);
     _mutex.lock(); 
     bool ret=MysqlQuery(_mysql,sql);
        if(ret==false){
          _mutex.unlock();
          return false;
        }
        MYSQL_RES *res=mysql_store_result(_mysql);
        if(res==NULL){
          printf("store result failed!:%s\n",mysql_error(_mysql));
          _mutex.unlock(); 
          return false;
        }
        _mutex.unlock(); 
        int num_row=mysql_num_rows(res);
        if(num_row<1){
          std::cout<<"have no order:"<<order_id<<std::endl;
          return false;
        }
          MYSQL_ROW row=mysql_fetch_row(res);
          (*order)["id"]=order_id;
          (*order)["dishes"]=row[0];   
          (*order)["status"]=atoi(row[1]);   
          (*order)["ctime"]=row[2];  
          (*order)["mtime"]=row[3];
  
          mysql_free_result(res);
          return true;
      }
  };
}
